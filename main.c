#include <stdio.h>
#include <stdlib.h>
#define bb while(getchar()!='\n')
#define MAX_TEXT 100
#define MAX_TEXT2 500
#define MAX_REPETICIONS 100
#define MAX_CARACTERS 100
#define MAX_CODIS 100
#define MAX_DIGITS 5

int main()
{
    char codis[MAX_CODIS][MAX_DIGITS]= {"0","1","00","01","10","11","000","001","010","011","100","101","110","111","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111","00000"};
    char text[MAX_TEXT+1],text2[MAX_TEXT2+1],buscacodis[MAX_DIGITS],auxchar;
    char caracter[MAX_CARACTERS+1];
    int repeticions[MAX_REPETICIONS];
    int it,it2,ic,i,j,aux,ico,lng;
    it=0;
    it2=0;
    ic=0;
    ico=0;                      //INICIALITZACIONS
    lng=0;
    i=0;
    j=0;
    aux=0;
    text[it]='\0';
    text2[it2]='\0';
    auxchar='\0';

    for(it=0;it<=MAX_CARACTERS;it++)    //inicialitzar caracter
        caracter[it]='\0';

    for(it=0;it<MAX_REPETICIONS;it++)   //inicialitzar repeticions
        repeticions[it]=1;

    it=0;

    printf("Introdueix el text que vols comprimir i descomprimir: ");   //inicialitzar el text 1 amb scanf
    scanf("%100[^\n]",text);

    while(text[lng]!='\0')lng++;	//calcular longitud text

    while(text[it]!='\0'){
        ic=0;
        while(text[it]!=caracter[ic] &&
              caracter[ic]!='\0') ic++;
        if(caracter[ic]=='\0'){
            caracter[ic]=text[it];
        }else{
            repeticions[ic]++;
        }
        it++;
    }

    ic=0;
    while(caracter[ic]!='\0') ic++;


    for(i=2;i<=ic;i++){
        for(j=0;j<=ic-i;j++){
            if(repeticions[j]<repeticions[j+1]) {
                aux=repeticions[j];
                repeticions[j]=repeticions[j+1];		//metode bombolla
                repeticions[j+1]=aux;
                auxchar=caracter[j];
                caracter[j]=caracter[j+1];
                caracter[j+1]=auxchar;
            }
        }
    }

        printf("\nEl text �s: \n\t%s\n",text);	//sortida text

    i=0;
    it=0;
    auxchar='\0';

    while(text[it]!='\0'){
        auxchar=text[it];//copiar caracter de text per anar a per el codi
        i=0;
        while(auxchar!=caracter[i])i++;//buscar codi del caracter que hem agafat
        ico=0;
        if(auxchar==caracter[i]){
            while(codis[i][ico]!='\0'){
                text2[it2]=codis[i][ico];//copiar codi al text auxiliar per a poder mostrar la codificacio
                it2++;ico++;
            }
        }
        text2[it2]='$';it2++;//afegir dollar i sumar posicio al text2
        it++;
    }

    //restem 1 al it2 per a poder borrar el ultim $
    //i aprofitar per ficar un \0 ja que l'hem de ficar manualment
    it2--;
    text2[it2]='\0';

    printf("\nEl text compactat �s: \n\t%s\n\n",text2);


    it=0;
    it2=0;
    //iteracio per a comptar el nombre de bits del text compactats
    while(text2[it]!='\0'){
        while(text2[it]!='$' && text2[it]!='\0'){
            it2++;it++;
        }
        while(text2[it]=='$'){
            it++;
        }
    }

    printf("Bits del text: %d*8 = %d bits\n",lng,lng*8);
    printf("Bits del text compactat: %d bits\n",it2);			//calcular compactacio
    printf("%c de compactaci�: %d\n\n",37,100-((it2*100)/(lng*8)));

    printf("Taula (car�cter - cop/s que ha sortit - codi assignat\n");
    i=0;

    while(i<ic){
        if(repeticions[i]==1){
            printf("\t'%c'\t->\t%d cop  \t->\t%s\n",caracter[i],repeticions[i],codis[i]);	//fer sortir printf diferents, quan sigui el nombre de repeticions
                                                                                            //major de 2 ficara cops i quan sigui menor ficara cop
            i++;
        }else{
            printf("\t'%c'\t->\t%d cops \t->\t%s\n",caracter[i],repeticions[i],codis[i]);
            i++;
        }
    }

    bb;                                                     //buidar buffer perque introdu�rem un altre text
    printf("Introdueix el text compactat: \n\t");
    scanf("%500[^\n]",text2);


    /*
    it2=0;
    ico=0;
    i=0;                    //inicialitzar variables que utilitzarem per a decodificar
    ic=0;
    it=0;
    j=0;
    lng=0;

    //PRIMER INTENT
    while(text2[it2]!='\0'){                                //fins no final
        while(text2[it2]!='$' && text2[it2]!='\0'){         //copiar codi fins que arribi al proxim $ o al final
            buscacodis[i]=text2[it2];
            i++;it2++;lng++;
        }/

        buscacodis[i]='\0';
        while(buscacodis[ic]!='\0'){
            while(buscacodis[ic]!=codis[ico][ic])ico++;
            while(buscacodis[ic]==codis[ico][ic] && buscacodis[ic]!='\0')ic++;
            if(buscacodis[ic]==codis[ico][ic]){
                text[it]=caracter[ico];
            }
        }
        it2++;//saltar dolar




    //SEGON INTENT
        while(buscacodis[ic]!='\0'){
            while(buscacodis[ic]!=codis[ico][ic])ico++;
            if(buscacodis[ic]==codis[ico][ic] && buscacodis[ic]=='\0' || codis[ico][ic]=='\0'){

                ic++;
            }
            while(buscacodis[ic]==codis[ico][ic]){
                ic++;
            }
            if(buscacodis[ic]=='\0' || codis[ico][ic]=='\0'){
                text[it]=caracter[ico];
                it++;
            }
        }
    }
    */

    printf("El text descompactat �s: \n\t",text);


    return 0;
}
